---
title: "Dependency Injection Simply Explained Part III" 
date: 2017-12-19T15:52:22-05:00
draft: false
---

Now that we are armed with all the understanding that we need to get started
with Dependency Injection (DI), let's get to it. We’re going to use a continuation
of our construction/plumbing business invoice application from the first blog in
this series. I’m not going to walk through any of the implementations for this
code, we’re merely going to take a look at the container and how to set things
up from the beginning. The initial setup of the dependency container seems to be
something that can trip up a lot of people so I want to cover that really
quickly but I think that you can imagine how the actual creation of any invoice
would work from there. Let’s get started.

![image1](/images/dependency-injection-part3/image1.png)

This is the code snippet that we’re going to be going over for our wrap up. I
like it because it’s simple and easy to understand, yet shows off what is really
important. Just like in the previous blog post we are using our `main()` method
as the injection container for our application. I’ll start with why we would
want to do that. In our C# console application the `main()` method is the entry
point for our application, this is about as close to the beginning of the
application that we can get to. This will be different depending on what type of
application you are making, but generally you will want to determine your
dependencies as early as you possibly can. 

In our `main()` method we ask the user to provide us with what type of invoice
they would like to create. We use this as the driver for deciding most of the
dependencies that we are using, but we could use any number of switches to make
our dependency determinations. It could be the environment that we are in, a
config file, or any other way that you can come up with. I’m going to take a
quick caveat and talk about naming conventions. There are some people who might
disagree with how long some of my names can get, people who don’t like the way
the interfaces are named or any number of things. My personal preference is to
have names as long as necessary to be super descriptive. I use an IDE that has
autocomplete so it is rarely a problem. For the most part I like my interface
names to say what they should do and the implementing class to be named like it
is doing what the interface specifies. What’s most important is that you find a
way to name your variables/classes/interfaces that you like and you are
consistent in the use of it. If you are working on a team make sure everyone is
on the same page and go from there. I just wanted to bring it up because it’s a
discussion I’ve had many times. Moving on.

We’ll start from the bottom, procedurally speaking, but the top of our object
hierarchy, our Invoice classes. We have an `IInvoice` interface that has two
implementations, the `ConstructionInvoice` and `PlumbingInvoice`. In the code
above, we can see that the constructors for the `IInvoice` implementations take
a few arguments. These are the *dependencies* that our invoice objects need and
use. They require a database, a customer manager, and a product manager. These
three dependencies that are injected into the invoice are determined right above
the creation of the invoice object. I hope you can see how handy this is, you
can use some sort of switch to determine not only what type of invoice you need
to create, but what database gets used, how customers and products are managed.
All just by simply injecting the right implementation into the invoice object. 

I’m sure you noticed that two of those dependencies, the product and customer
managers, themselves have a dependency injected into their constructors. This
brings up an important topic when using DI, dependency chains. Both of our
managers require a database connection as a dependency, but our invoice object
does too. Not only that, but the invoice object also has a dependency on both
managers. I bet you can see how an application that is dealing with more than
just a handful of classes could run into a problem where it gets harder and
harder to provide a dependency without affecting its place in the chain. There
is no fool-proof way of dealing with this other than to keep your classes as
tight knit as possible. Any time you are having issues with circular
dependencies or instances where you must drastically change around your
dependency chain then that is usually a good code smell that your design isn’t
quite right. 

But that’s it. That is a quick look at setting up your dependencies. I’m sure
from here you realize that our `InvoiceMaker` class probably uses the invoice
that we provide it to create an invoice and you would write your code just like
you always do. We are just simply determining our objects up front rather than
newing up an instance of the object as needed. I hope this has been helpful to
clear up some of the scariness of DI. There are a ton of articles and blogs
available on the internet that can really take you to the next level of
understanding DI. My intent wasn’t to have you be an expert after reading these
blogs, what I really wanted to communicate is that you already understand how to
use DI. It’s not different, it’s not hard, and it’s not even new. I hope that I
have achieved that.



[Previous Page](/blog/dependency-injection-simply-explained-part-ii/)