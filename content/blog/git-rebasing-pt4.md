---
title: "Git Rebasing Simply Explained Part IV" 
date: 2017-10-24T15:55:22-05:00
draft: false
---

We’ve successfully started the rebase and we’ve run into our first conflict.
This is actually a good thing though. When we created our pull request we ran
into a bunch of conflicts that were preventing us from merging into the shared
develop branch, and this is when we get to start fixing those issues. We found
our first two problem files, so now let’s open them up in our IDE and fix them.
I’m going to be using Jetbrains’ *Rider* as my IDE but you can use *Visual
Studio* or whatever you are comfortable with. Here is what the `IDisplay.cs`
file looks like.

![image7](/images/git-rebasing/image7.png)
 
We have two sections that Git has marked for us, `HEAD` and `added closing
message`. `HEAD` is what exists on the remote repository and `added closing
message` is the first commit that we have in our `my-branch` feature. You must
remove the Git markers before the code will compile and these markers show the
different versions of the code area that are causing the conflicted state.
Looking at this we can see that someone else decided to add a greeting message
to our application. If you don’t know what a change is doing this is a great
time to find out who committed that change and ask them about it before making
any decisions on the validity of the code. This is easy though and we’ll pretend
that we knew this was happening. We want to have both a greeting and a closing
message. To fix this we would simply remove all three of the lines with the
Git markers on them to keep both methods. 

![image8](/images/git-rebasing/image8.png)
 
That was easy enough so let’s move on to the `DisplayOnConsoleFile.cs`. 

![image9](/images/git-rebasing/image9.png)
 
We could have probably guessed that the `DisplayOnConsole` class implements
`IDisplay` and since we had a change in the interface we’ll probably have the
same change in the implementation. Sure enough, we can see that the
`DisplayGreetingMessage` method was added to the file. Here is where I’d like to
make a digression for a minute. Most people see conflict resolution as simply
choosing which version of the code to keep, or whether to keep both. What’s
important to remember is that this is a great time to make some refactors based
on what other people’s code is doing. In this case, whoever added the
`DisplayGreetingMessage` method also added another method called
`DisplayMessage` which kind of abstracts out the responsibility of writing to
the console. We hadn’t thought of that when we wrote our code but now that we’re
seeing it here is absolutely makes sense to make that change in our code.
Because with a rebase we are rewriting our history there is no reason why we
can’t go ahead and make that change right now while we’re doing the rebase. Even
though we want to keep both branch’s changes, we can make a few modifications.
Something like this.

![image10](/images/git-rebasing/image10.png)
 
So, we changed our commit to incorporate this new method that the other person
added and rearranged things a little bit. Always remember that you are
rewriting history as you are applying your commits. It is perfectly valid for
you to adjust things to make the code as clear as possible. After we fix and
save this file that takes care of our conflicted files for this step of the
rebase. The next thing we’re going to want to do is run our unit tests and make
sure that those all still work. 

![image11](/images/git-rebasing/image11.png)
 
Everything looks good with our unit tests so now it’s time to continue with the
rebase. The next step is to add the files that we have made changes to. Going
back to our terminal we’ll add our changed files and continue with the rebase. 

![image12](/images/git-rebasing/image12.png)
 
> Commands Run:

> `git add .` (add all of our changed files)

> `git rebase –continue` (the continue flag tells Git to move on to the next commit to apply)

First thing to notice is that we are now magically applying commit four (bottom
right of the screenshot). OK, so it isn’t magic. I mentioned before that Git
won’t need you to help it every time when it is applying your commits. In this
case commits three and four were able to be applied with no interaction so Git
doesn’t even bother telling you about them. On commit four we have a couple of
issues again. To find out what’s going on we’ll follow the same template as last
time and get the status of the branch.

![image13](/images/git-rebasing/image13.png)
 
> Commands Run:

> `git status` (get the status of our branch)

Looks like we have some problems with our `StockListRetriever.cs` and
`StockListRetrieverTest.cs` files. I won’t bore you by working through how to
fix these problems. The code itself doesn’t matter at all. I could have used a
couple lorem ipsum text files to create these conflicts. It’s up to you to know
how to change the code so that everything is properly in place. In this case it
was just removing some functionality from the `StockListRetriever` class and
adding it to our `StockManager` class. Just like last time we are going to be
*deliberate* and *methodical* in understanding the changes we are making. Once
we have our two files fixed up we’re going to run the unit tests to makes sure
that they pass. They do and since this is our last commit to apply it’s a good
idea to run the application itself and make sure that everything works as it
should. 

![image14](/images/git-rebasing/image14.png)
 
It randomly chooses ten stocks for us to invest in and also displays a greeting
and closing message. Since everything looks good it’s time to finish up the
rebase. We’ll do that by adding our changes and then running the `rebase`
command one more time with the `--continue` flag. Next, we’ll go ahead and look
at the status of the branch.

![image15](/images/git-rebasing/image15.png)
 
> Commands Run:

> `git add .` (add all of our file changes)

> `git rebase –continue` (continue with the rebase)

> `git status` (show status of the branch)

This should seem interesting to you, when we run the `git status` command we see
that we are out of sync with our remote branch. Git is telling us that we have
five commits in our local `my-branch` and the remote `my-branch` has four
commits. If we stop to think about this though it would make sense that this is
true, we incorporated that one commit from the `develop` branch. Logically it
makes sense that we’d have one extra commit. If we went ahead and tried to push
that up to remote we’d see a message like this. 

![image16](/images/git-rebasing/image16.png)
 
Git won’t let us do a push to get our version of the branch up to the remote
repository. This is where some people see a downside, you will have this
situation every time you do a rebase. Git thinks that our local branch is behind
the remote repositories version and there’s only one way to fix this, and that
is with using the `--force` or `-f` flag with our `push` command. Let’s take a
moment to talk about force pushing though before we just continue with it. A
force push is kind of like a table saw. It’s a tool that is extremely useful,
but you have to be deliberate and aware of the dangers. With a table saw the
consequence of not using it properly is having a board kick back into your face
though. Maybe just overwriting your remote version of your code isn’t too bad.
That overwrite will happen though when you use a force push. It’s not maybe, or
probably, or mostly will. When you use the `-f` flag when doing a push it
absolutely will overwrite what is on the remote branch in the repository with
your version of the branch and its history. I promise though that it won’t mess
things up randomly, just like table saws don’t kick back randomly. People use
table saws every day and don’t hurt themselves, just like people use force
pushes every day and don’t mess anything up. You just have to know what you are
doing and be cognizant of the consequences of doing it. We are confident that
our version of `my-branch` that we have locally is what we want up on the remote
because we just went through the trouble of rebasing and making sure of that.
We’re going to go ahead and do a force push.
 
![image17](/images/git-rebasing/image17.png)

> Commands Run:

> `git push origin -my-branch -f` (pushing our branch up to the remote repository using the -f flag)

> `git status` (show the status of our branch)

You can see that this time Git was cool with our push and didn’t ask any more
questions about it. After we did the force push we ran the `git status` command
and sure enough the remote `my-branch` matches our local `my-branch` this is
exactly where we want to be. We can go ahead and create a pull request based off
our remote `my-branch` branch and we won’t have any conflicts. First though I
would highly recommend doing an interactive rebase to squash all of our commits
down to one, but I won’t walk you through that. 

If you break down what we did it’s really not any harder than doing a merge. The
most time-consuming part of the rebase is dealing with the conflicts. You’re
going to have those same conflicts when doing a merge though so I wouldn’t count
that as a negative. We do have to deal with them commit by commit since we apply
each commit with a rebase. To me this is a benefit because it makes it easier to
make those deliberate decisions about how we want the code to change since the
conflicts come one at a time. We had to do a force push at the end but as we
just saw if you make your changes in a logical way then you should feel very
confident doing a force push. So, that’s no big deal. The benefits will really
show themselves when you look at the repository history and it actually makes
sense. It’s nice to see a branch split off and then come right back in. You can
easily look for a change and you see all of the information that you really want
to know. Lastly, if we had made a mistake in our feature branch, we can easily
revert that back out, make a change, rewrite the history with a rebase and put
it back in. Merging is like a circular saw; most people have used one and are
really comfortable with it. It’s a good tool, but once you use a table saw you
quickly realize that it makes your life so much easier and find yourself using
the circular saw in rare situations rather than the other way around.

 


[Previous Page](/blog/git-rebasing-simply-explained-part-iii/)