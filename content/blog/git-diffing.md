---
title: "Git Diffing"
date: 2018-01-16T13:37:45-05:00
draft: false
---

At work we recently had a situation where we lost reliable access to our remote
repositories for a couple of days. As an apprentice this was annoying because I
finally got that dang minimax algorithm working and then I couldn’t get my pull
request reviewed and approved. For our production teams though, this is a very
large issue. Our repositories are very active and losing access to them for even
a short amount of time is really detrimental to our normal workflows. Now, in
this instance, the web interface that we normally use to interact with the
repositories seemed to take the biggest hit. We would lose access to the repos
intermittently through the Git command line, but the web interface is where the
larger problem seemed to be. A mentor suggested that this would be a good topic
for a blog post, since my sprint was almost derailed because of the lack of
access to the web interface. I’m going to focus mostly on how we could have
reviewed pull requests through the command line and given some verbal approvals.
This would have allowed us to continue to operate in a manner similar to what
we’re used to.

Let me start by explaining what our normal Git process is so you can see how
things would be affected by the outage. We have a mixture of people who use the
command line and people who use a visual application to manage their branches.
My normal procedure is to create a branch (obviously), commit code throughout
the process of working on whatever I’m working on, then push up to the remote
repository. I’ll then typically do a rebase to squash all of my commits down to
one, get on the web application to verify that the code changes make sense, and
then create a pull request from the web application. The people that I put as
reviewers on the pull request will eventually review the code and suggest
changes or issue an approval, also through the web interface. After two
approvals I am then able to merge that branch and the process is finished. The
situation I found myself in is that I had the pull request created, but nobody
could review it because the web interface wasn’t working. Instead of trying to
work around this I had to wait until the web application was working again and
almost didn’t get enough approvals for my hard work in time for the end of the
sprint.

The thing you need to do first and foremost is keep your branches up to date!
Don’t go days without doing a fetch and pulling down the latest changes that
have happened on the repository. If you pull down changes multiple times a day
then you can be sure that any new branch you create won’t be that far out of
date. When merging your newly created branch back into the working branch later
you won’t have to resolve as many conflicts. To go along with this, make sure
you push your local commits up to the remote repository from time to time. It's
just a good idea. Let's pretend you are in the position that I recently found
myself. You’re at the point where you’ve created your pull request and are just
waiting on your coworkers to review and approve. It can be painful to get people
to review your pull requests in the best of times and now the web interface is
down. If you do what I did you just admit defeat to the internet gods and hope
that they allow the servers to come back up in a timely manner. What I'd propose
you can do instead is post some instructions on how to continue to review the
pull request in the developer chat channel and bug people until they do it. 

You hopefully know the name of the branch that you are working on and created
your pull request from. I also assume that you know the working branch that you
are wanting to merge your pull request into. If this is the case, then you can
easily tell the people who would be reviewing your pull request to run a `git
diff` command in their command line. In this example, the pull request is
derived from our `test-pr` branch and I want to merge this branch into the
`develop` branch that everyone shares. I’ve made sure to do a `git fetch` and
then a `git pull` on my `develop` branch to ensure that everything is up to
date. Next, I’m going to use the `git diff test-pr develop` command. What this
is doing is displaying the differences (diff) between the `test-pr` and
`develop` branches. This is essentially what is happening when looking at the
diff in the web interface. Below I have made a bunch of fake changes for an F#
Tic Tac Toe game (I wonder where I found that). I just wanted to illustrate what
`git diff` looks like in the terminal.

 ![image1](/images/git-diffing/image1.png)

I’ve made some changes on my `test-pr` branch from what is on `develop` and this
is how they are displayed in my console. Does this look as nice as it does on
the web interface? Not at all. Is it perfectly functional? Yes, it is. There are
tons of plugins that you could download if you wanted to display this diff in a
prettier way, but I don’t do it that often with big changes. I would now give my
colleagues the branch to compare and what to compare it against and how to use
the `git diff` command. After they have looked through the changes, I can then
get verbal approvals and then use the command line to merge my `test-pr` branch
into `develop`. It’s important to remember that these web applications used to
manage code repositories are mostly there to make things easier to use. Almost
anything that you can do through the web interface can be done through the
command line and you will almost certainly have to use the command line at some
point. This is why I personally encourage everyone to learn to use the Git
terminal. At the end of the day, it would have been nice to provide a way that I
could still get my pull request reviewed even with the web interface down. I
could have gotten the verbal approvals and merged everything in just as I
normally would. Instead my code barely made it in before the demo. I definitely
would not use the Git terminal as my preferred way of reviewing pull requests,
but it's always nice to have a backup tool available for emergencies. 
