---
title: "Dependency Injection Simply Explained" 
date: 2017-11-03T15:52:22-05:00
draft: false
---

Trying to come up with a topic for this, I tried to think of the question that
I’ve been asked most frequently at work. The first that comes to mind is
probably an Office Space-esque, “What would you say… you do here?” I don’t want
to answer that, so next question. Next in line would probably be, “Do you
understand this Dependency Injection stuff?” I don’t want to answer that so… I’m
just kidding, that’s one I think I can answer. Because I love multi-part blogs
and they make everyone else roll their eyes this is going to be another one of
those. This first part is just going to be a brief explanation of what
constitutes Dependency Injection (DI) and why it’s just a fancy word for
something you already understand. Subsequent posts will take a dive into some
OOP concepts about abstraction and the reasons people use DI, and last will be
some examples of using DI to provide our classes with their dependencies.  It’s
going to be fun and hopefully clear some things up.

To begin with, don’t get caught up on the term *Dependency Injection* because
I’m positive you already use it. If we break it down, what is a dependency?
Well, it’s just something that an object *depends* on to function.  Unless you
work on an application that has all its code in one class and no external
libraries your code already uses dependencies. Now, what about the injection
portion of it? You’ve probably been doing this also. I’m sure you have a method
or function that takes an object as a parameter because you need access to the
instance properties of that object. Congrats, you are *injecting* that object
into another. More than likely though, if you aren’t using DI, you are doing it
the “bad” way that DI tries to prevent. This would be declaring a bunch of new
instances of an object whenever you need them and not using abstractions. Take
the following code as an example, you might do something similar.

![image1](/images/dependency-injection/image1.png)

Here we are getting our invoice information and then creating a new invoice and
saving it. This is an example of the *dependency* part of DI, but not the
*injection* portion. Our `InvoiceMaker` class has a dependency on `Invoice` for
it to complete its operations. Let’s say we’re down with abstractions though and
we don’t want to have to declare a new instance every time. Pretend that we work
for a construction business that does both general construction and plumbing.
Our company has two different invoices depending on which business space we’re
operating in, but they’re both generally the same. The only differences in the
invoices are that we have separate invoice number counters and save to two
different databases depending on which business space we’re operating in. We
made the decision to make `Invoice` an interface and have two separate
implementations. The `InvoiceMaker` class is going to be making both types of
invoices. If we think about it logically, at some point we need to let it know
which type of invoice it is getting so the invoice gets saved with the correct
invoice number and to the right database. We can handle this by *injecting* the
correct implementation into our `InvoiceMaker` class. Let’s look at what that
might look like.

![image2](/images/dependency-injection/image2.png)

Our `InvoiceMaker` object will now take one of the implementations of `IInvoice`
(yes, saying IInvoice out loud makes you sound like you have a stutter) as a
parameter. Guess what, we’re *injecting* that *dependency* into our object.
We’re using dependency injection! What’s important to remember is that it really
is that simple. Obviously, things can get more complicated, but that’s true of
any programming topic. Let’s take this a step further and show how we would use
this. We can imagine that at the beginning of our program we will want to ask
the user what type of invoice they are trying to make and then use that
information to provide the correct dependency to our `InvoiceMaker` object. It’d
probably go like this:

![image3](/images/dependency-injection/image3.png)

In the `main` method we are asking the user if they are making a construction or
plumbing type invoice and give our object the correct implementation. We simply
moved the determination of our dependency up the chain of execution. The `main`
method is the entry point for our program so this is a great place to inject our
dependencies. In fact, we should figure out all our dependencies that we’re
going to need here at the beginning and inject them all at once. We could call
the `main` method our dependency injection container! The law of next logical
steps says we could even use config files to inject different things based on
our environment and on and on. 
 
Broad, sweeping statement here, but you already know how to do all of this. I’m
assuming nothing I showed you here is new to anyone who has asked me about
Dependency Injection. Don’t get confused by the term itself. At its heart DI is
just about providing your objects with their dependencies up front, rather than
creating them on the fly. In my next post we’re going to talk a little bit about
why DI is so useful and make sure we have a good understanding of abstractions
which is necessary when using DI. See you soon!



[Next Page](/blog/dependency-injection-simply-explained-part-ii/)