---
title: "Object Oriented Design" 
date: 2017-10-12T13:36:35-05:00 
draft: false
---

I’ve been reading [Practical Object-Oriented Design in Ruby
(POODR)](http://www.poodr.com/) by [Sandi Metz](https://www.sandimetz.com/)
lately. I’ve only made it about half-way through so far, but I can already tell
it’s one of those books that is easy to form opinions on. Just the introduction
will probably give you some things to sit back and think about. Because it’s on
my mind I want to take a moment to talk about Object-Oriented Design (OOD).
POODR is a book though, and this is a blog. I’ve struggled with how to take a
complex idea like OOD and fit it within the confines of a blog. I don’t have
enough space to discuss just Liskov’s Substitution Principle or the Dependency
Inversion Principle in any coherent form, let alone everything else OOD
encompasses. I want to write about it though because I love the thought of it
and what OOD represents. I like writing my blogs through a lens of simple
explanations for complex problems and ideas. It probably helps me more than
anyone reading, but it’s what makes me enjoy writing them. So, we’re going to
try to explain OOD in a simple way. 

If you want to read the [official definition of
OOD](https://en.wikipedia.org/wiki/Ood) then by all means go and visit Wikipedia
right now and come back for the short form. You’re probably aware of what
Object-Oriented Programming is. It’s the good stuff like classes, encapsulated
properties and methods, inheritance, and class hierarchies. OOP revolves around
objects and those objects passing messages back and forth between each other to
achieve a desired result. OOD is about *designing* how those objects are going
to interact with each other. That should be a hint that OOD takes place before
OOP, we need to of course design something before we can program it. Just as the
product you’re making should be designed before you start working on it, your
code needs some forethought and design before you can write it. Thankfully there
are design principles and patterns that very smart programmers have developed
over the years that will give you a good base of understanding of the design
choices you need to take into consideration. 

---

>For more information about design principles, take a look at the [SOLID
>principles](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)). 

>For more information about design patterns, the best place to go is the [Design
>Patterns](https://en.wikipedia.org/wiki/Design_Patterns) book by the Gang of Four (Erich Gamma, Richard
>Helm, Ralph Johnson, and Jon Vlissides).

---

That’s really it though, it sounds so simple. Just design your code before you
write it. It’s obviously nowhere near that easy and there’s a lot to consider
when writing code in an Object-Oriented manner. As an example, I’ll use
something we’re all intimately familiar with, the human body. Think of
everything in your body, your heart, brain, lungs, appendages and everything
else. Imagine that they are all objects, most of them with a ton of sub-objects
in their hierarchy. Human 1.0 is an incredibly complex program. Now it’s come
down from way up high that a ton of research has gone into Human 2.0 and here
are the requirements. The process of taking those requirements, use cases, and
model relationships and designing how these objects are going to interact with
each other in a safe way that still satisfies the requirements is OOD. You can't
just feel your way through Human 2.0 as you write it because it is way too
complex. We're going to need to design things like how the brain object is going
to pass messages to the spinal cord object to forward on to the list of motor
neuron objects. They then need to send impulses to the muscle object which is
how we will eventually get the output we wanted, which is to get an appendage
object to move. You don't want to have the brain class sending messages directly
to the muscle class because it won't know what to do with those types of
messages. Making sure that everything interacts with everything else in a
logical way but with a loose coupling of dependency is a big part of OOD.

You may not realize that the human body changes all the time, but it does. Of
course, the ability to age was already a requirement for Human 1.0 so I’m not
talking about that. Our body deals with and responds to changes in our
environment. The introduction of new diseases, sedentary lifestyles, and the
availability of food all force physiological changes to take place within our
body. Human 1.0 was coded in the early days and of course the programmer wanted
to write good code, it’s just with the deadlines and stuff it didn’t turn out
the best. Human 1.0 is a real bear to make changes to and they take forever.
Whenever the body is presented with these environmental changes it is always
behind the curve for when those changes would have been nice to have. 

With Human 2.0 you’re going to make it easy to change because you know that no
set of requirements is ever truly finished. Use cases change and new ones pop up
all the time. This is the main problem that OOD solves. If you knew that the set of
requirements you received were 100% complete and that Human 2.0 would never
change then you don’t need to worry about OOD. It’s actually pointless to you.
Sure, you could use some of the design patterns and principles to make your code
better. If it never needs changed or maintained though, and as long as there’s
no performance issues, it doesn’t really matter. There’s a reason you were
chosen for Human 2.0, you’re smart and you know that of course Human 2.0 is
going to need to be maintained. That’s why extensibility and reusability is the
other core tenant of OOD, your program must allow for easy changes. 

The ability to extend or easily change Human 2.0 instead of spending outrageous
amounts of time refactoring tightly coupled code is why this rewrite is
necessary in the first place. Coincidentally it is where you will save the most
time in the future. By creating Human 2.0 with as little direct dependencies as
possible you are setting yourself up to have an application that will be easy to
change as the needs of the application evolve in the future. Keep that in mind
as you work on your project, and as a Human 1.0 model, please remember that the
head object is supposed to have a list of hair objects. Human 1.0 seems to lose
that dependency sometimes.
