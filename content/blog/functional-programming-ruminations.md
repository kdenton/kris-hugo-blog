---
title: "Functional Programming Ruminations"
date: 2018-01-09T13:37:45-05:00
draft: false
---

I’ve finally finished up my F# Minimax implementation for my Tic-Tac-Toe game
that I spoke about in my [last blog](/blog/minimax-and-me/) and I think everything
eventually ended up in a good spot. It seems to be working great and since I am
wrapping up my foray into functional programming I felt that this would be a
good time to do a little reminiscing about what I’ve learned. I’ve never done
any functional programming before this part of my apprenticeship and I can’t
believe how far I’ve come in a month and a half. I would consider myself more
comfortable with OOP languages, but I now feel confident with being able to work
through a functional program and it has shifted my view of how to approach
problems. Everything I’ve learned while doing this project will help me even
when working in the OOP sphere and has been a valuable experience for me.

The F# syntax, lack of objects, and immutable variables were a lot to take in at
first and I was very resistant to these things. Just getting used to the lack of
curly braces took way more effort than I would have imagined. I felt like I was
never going to get used to everything, but now I can just type F# syntax nearly
as well as C#. Which brings me to my first point, the F# compiler is awesome! I
never really thought about it until I started working with F#, but so much of
what I’m typing when doing my typical C# application is just fluff. The ability
of F#’s compiler to infer typing in my application has been eye opening for me
and I really like how smoothly it handles most situations. I’ve rarely run into
scenarios where I need to give it a type for longer than it takes me to finish
out the function. It’s really nice and although I started out very resistant to
the differences in F#’s syntax, now I wouldn’t mind if I didn’t have to type
another curly brace in my life.

I am in love with the conciseness of my F# application and I think that this is
one of the most amazing aspects of functional programming. I’m going to throw
some numbers out real quick and I think they speak for themselves as far as F#’s
ability to shrink down an application. I wrote this exact same program in Java
before I started on my F# implementation and I had 17 files with about 840 lines
of code. With my F# Tic-Tac-Toe I have 2 files and 272 lines of code! That’s
about a third of the size when it comes to lines of code and obviously a huge
reduction in the number of files needed. I should also mention that although I
technically have 2 files in the F# application, one is just the entry point for
the application, the vast majority of the code is in one file. The amount of
time saved by no longer searching through files to find things or jumping
through a million declarations to get somewhere is refreshing. Coming from an
OOP background it never really occurred to me that it could be another way, so
it was cool to see that come out of this. 

Building this functional program really helped me gain a better understanding of
some concepts that I use all of the time in the OO paradigm. Things like
lambdas, recursive functions, and the LINQ library are all things that I feel
like I understand that much better now. I’ve had to use all of these before
working on this application and I was always able to get by with what I knew.
Lambdas and recursive functions are things that I leveraged heavily in my F#
application and I feel like I now have a much deeper understanding of how to
properly utilize them. I will say that this especially applies when talking
about them in the context of `List` operations. I didn’t really use much LINQ in
my F# application, but the whole library is basically functional programming and
I can see how to use it much more effectively now. 

Even with all of these great things that have come out of my introduction to
functional programming I won’t say that I am a complete convert just yet.
Perhaps this is just because of my relative unfamiliarity with functional
programming, but I can’t quite shake the feeling that it isn’t how I naturally
think. I feel that OOP is easier for me because I think of the world as objects
and interactions anyways. I can understand that a person object would have a
name property or a list of children of the person type. In my mind, those sorts
of descriptions and interactions are easy to pick out and design because they
exist in the real world. Writing in a functional manner just doesn’t naturally
occur to me like that, so that’s going to take me a while to get over. Other
than that, I am quite taken with F# in general. 

I’ve really enjoyed getting the chance to explore this completely other
programming paradigm that I’ve had zero experience with up until this point. I
would really recommend taking some time to look at it yourself, and at least
give it enough time to get over the syntactical differences. They can be quite
jarring at first but you get used to them quickly. I went into this project
expecting that I was going to learn at least the basics of functional
programming, that it would be difficult for me, and that I probably wasn't going
to like functional programming. I was right about the first two, but I'm
surprised at how much I do enjoy writing functional code. There is something
about functional programming that seems spartan and since I'm kind of a
minimalistic person that appeals to me. I don't know what the future holds for
me as far as being able to use F# at work, but I would love to do some fun side
project with it. It really is that fun.
