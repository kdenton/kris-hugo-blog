---
title: "Git Rebasing Simply Explained Part II" 
date: 2017-10-24T15:53:22-05:00
draft: false
---

This is the second part of a series of blogs detailing Git rebasing. If you
haven’t read the first part about why you should use rebasing in your workflow
then I would suggest that you do so now. If you don’t want me to tell you how to
live your life then by all means continue reading from here. 

> All visualization come from the Atlassian tutorial series located
> [here]( https://www.atlassian.com/git/tutorials/merging-vs-rebasing). Atlassian
> does a great job of explaining concepts in their tutorials so I would check
> those out too. Start there if you don’t even know what Git is and want to know a
> little more about that.

Now, maybe you work at a weird company that does weird things in a weird way for
weird reasons. I don’t know how to explain this to match your workflow so you
may have to make some adaptions. You may also be the sole developer working in a
repository, at which point none of this is going to really matter to you. Most
people though will almost always be in a situation like this.

![image1](/images/git-rebasing/image1.png)
 
You will have a common branch that people work off of, in this case `master`,
and you will create a `feature` branch to start on a feature. Each one of these
dots represents a commit, so in the above picture there were a couple commits on
`master`, we made a branch called `feature` and made some of our own commits on
that branch. Meanwhile, as we’re working, other people are working on their own
feature branches and have merged a couple of those back into master. This
results in what is called a *forked history*.

Now, after our three commits we feel like our work is complete. Our unit tests
pass and everything works just as we would expect. This is when we would push
the latest changes on our local `feature` branch up to the remote repository.
The next step in a “normal” workflow would be for us to then open up a pull
request to get our branch merged back into `master`. This is the point that we
would more than likely look at the diff for the merge and realize that we have
some conflicts. You know it’s not your branch because you started with `master`,
made your changes and everything is great locally. We therefore know that some
changes have happened to `master` in the meantime. 

This is usually when you get a little frustrated with your co-workers and wonder
why it seems like they’re always purposefully sabotaging your feature work.
Whatever their reasoning may be, we have to deal with their changes so time to
buckle down and figure out how we’re going to handle this. Here is where we have
a choice, we can either do a merge or do a rebase. Let’s take a look at a merge.

![image2](/images/git-rebasing/image2.png)
 
As you can see we can just simply take what is on the `master` branch and merge
it into our `feature` branch. The differences between the two branches will be
combined and when we create our pull request back into `master` the diff will
only show the changes that we have made. You’ll notice though that we have an
extra commit on our branch. This is the merge commit that I talked about in the
first part of this series. This merge commit has to stay because it contains the
changes from the two commits that were added to `master` since we started our
`feature` branch. We may have had to resolve some conflicts, but now we’ve got
those changes from `master` in our branch and we are ready to merge our
`feature` branch back in. We’re ready to go, and as I’ve said, this is a valid
way to handle this. We have this extra commit though. Let’s suppose that it
takes us multiple days to work on our feature and we’re bringing in the changes
on `master` a couple times a day because we don’t want to deal with all of the
conflicts of a massive merge at the end. By the time we are ready to merge our
`feature` branch back into `master` we’re going to have quite a few of the
extraneous commits. That’s not ideal to me, which is one reason why I prefer a
rebase.

![image3](/images/git-rebasing/image3.png)
 
Compare this to the previous illustration. It looks quite different because it
is, but it’s important to remember that the end result is exactly the same as
when we do a merge. We are incorporating the commits made on `master` since our
`feature` branch began so that we can create a pull request without any merge
conflicts. Let’s take a little bit to talk about what exactly is happening here.

Something I’d like to point out is that your branch is just your commits. People
sometimes think of their branch as the summation of all of the code in their
local branch. Git merely tracks the changes that are made. That’s the entire
point of commits, I made these changes, please keep track of it. Once we realize
that our `feature` branch is just tracking the changes that we’ve made, why
can’t we merely just pick those changes up? We’ll place them to the side for a
minute, update our `master` branch, and then plop them right back down on the
tip of the `master` branch. If the file is changed completely so that it just
prints out “Test” to the console, your changes are still valid. They may not
make any sense anymore and that will be a fun conflict to resolve, but they are
still the changes that you intended to make and can be applied to that file.

You’ll realize that we still only have three commits with the rebase. We didn’t
generate that extra merge commit because we are rewriting the history for the
repository. There is nothing in the history that says that the `feature` branch
was started after the first two commits on the `master` branch originally. As
far as the repository will ever be aware, the `feature` branch was always
started after the fourth commit on the `master` branch and was merged in right
after that. We literally picked up our changes and put them onto the tip of
master. This is something else to consider when thinking about the difference
between rebasing and merging. When you merge you are bringing the changes from
`master` *into* your `feature` branch. When you rebase, you are putting your
`feature` branch changes *onto* the end of the `master` branch. 

In this example we have three commits on our `feature` branch, but we should
make those commits into one “atomic” commit by squashing all three of those
together into one commit before the pull request. This will make the project
history much cleaner because the history will show that the feature was added
but not all of the little commits you needed to make. 

In my experience, I make a ton of little commits because I work on a team and
sometimes I need to push things up to the remote branch so someone else can do a
pull and look at something. I may be pair programming with someone else on the
team so we’re constantly making changes and pushing up and passing the code back
and forth. You have to make these commits, but they don’t really represent
anything important. You’re just working in a distributed manner like Git was
designed for. By squashing these commits you aren’t hiding history or anything
like that, those commits just aren’t important. When you merge changes though
you can’t escape this, and on top of that every time you merge you have another
merge commit too! I’m not lying when I say that it can muddy up the history a
lot for no reason. Then, if a change sixteen commits back causes a problem when
you merge back into `master` imagine how hard that will be to dig out and fix. 

Rebasing has a reputation as being difficult, but in my opinion, it is not any
harder than merging. Another common trope is that it hides history. My challenge
would be to justify why my commit that only added a unit test so that I can pass
it back to a team member to implement is important to the repository. The
feature that was added is important, the twenty individual commits that make up
that feature aren’t worth mentioning to me. 


[Previous Page](/blog/git-rebasing-simply-explained/) | [Next Page](/blog/git-rebasing-simply-explained-part-iii/)