---
title: "Minimax and Me"
date: 2018-01-03T13:37:45-05:00
draft: false
---

Have you ever seen the movie [Marley and
Me](https://en.wikipedia.org/wiki/Marley_%26_Me)? From what I remember of the
plot, a couple decides to get a puppy to see if they’re ready to have children.
The dog is untrainable and so difficult that he is a trial on everyone and is
constantly causing issues. He eventually dies and everyone realizes that he was
a great dog or something like that. My life kind of feels like that right now,
and I’ll explain why. For my apprenticeship (Marley) here at work the theme is
Tic-Tac-Toe, you know, the game that children play and is really simple. My
first implementation of the game was written in Java and like I said,
Tic-Tac-Toe is simple so I didn’t think it would be a problem. I was wrong, and
it was hard for me. Granted, the main point of the apprenticeship is teaching
skills like design, unit testing, and clean code patterns. I may have been able
to just bang out a Java Tic-Tac-Toe project that worked pretty well but wasn’t
pretty and skipped most of the problems. That would be against the ideals of the
apprenticeship though and it took me what felt like forever to get through that
in an OOP language (my comfort zone). I made it with my own “algorithm” for the
AI that was just a bunch of functions that followed the Tic-Tac-Toe heuristics
and everything felt good and I thought maybe my puppy was finally starting to
not be difficult.

I was wrong of course. In the movie Marley never gets any less difficult and
neither does the apprenticeship. I’ve since moved onto rewriting my Tic-Tac-Toe
game in [F#](http://fsharp.org/), a functional language. I have absolutely no
experience in functional programming and that is why, I assume, I have to write
my Tic-Tac-Toe game in one. Whatever, I’ve used the LINQ library in C#, it’s
basically the same thing I thought. Turns out functional programming really is a
completely different programming paradigm and I’m back on the struggle bus. I’ve
spent weeks learning how to design and write an application with no classes,
mutable data types, or for loops. On top of that, all of this is in a new syntax
so I have to google most things before I write them. I’ve been doing it though,
I’ve gotten to the point where I can now write F# fairly quickly, it’s making
sense and I’ve finally got that currying and pattern matching stuff figured out.
I felt like my personal Marley was finally starting to shape up and quit making
my life so stressful. 

You would think I’d learn by now, but of course the story isn’t on to the
happily ever after part yet. I think that only happens when Marley dies and you
can look back with those rose-colored glasses. My mentor thought it’d be a good
idea to implement the Minimax algorithm this time around and I agreed it’d be an
interesting way to get the AI portion of the Tic-Tac-Toe game done. I’d read a
little about it when doing my Java implementation of the game but decided that
I’d rather just follow the heuristic and build out the AI in a way that I’m
comfortable with. I’ll explain the AI portion of the game and Minimax for a
minute.

A requirement for this Tic-Tac-Toe game is that one player is a human and the
other is an unbeatable computer player. In order for the computer player to be
unbeatable you need some sort of algorithm that will calculate all possible
moves that the computer can take, accounting for possible moves from the other
player, and then determine the best move to make. In my Java application I used
the rules for the Tic-Tac-Toe best strategy found on the [wiki
page](https://en.wikipedia.org/wiki/Tic-tac-toe) for the game. It ended up
taking quite a while, but it worked and I was happy with it.
[Minimax](https://en.wikipedia.org/wiki/Minimax) is an algorithm usually
associated with game theory that tries to **mini**mize the possible loss in the
**max**imum scenario (worst case). It does this by traversing through all
possible moves and finding the move with the least loss. Because the algorithm
tries to determine all possible moves it is well suited to a two-player game
with a small board like Tic-Tac-Toe. It is less useful for larger games like
chess because of the sheer number of possible moves that must be exponentially
traversed. 

So, I originally thought Minimax would be hard but achievable. Looks like I was
right about something. Although it is achievable, it is also really humbling. I
didn’t go to school for Computer Science, so my knowledge of the common
algorithms is non-existent and is something I really need to rectify.
Understanding algorithms just hasn't been a concern so far in my career. I don’t
know if it was this fact that made Minimax so hard for me to understand or what,
but it has been at least a month since I’ve been forced to learn something that
has kicked my butt so completely. I’ve spent most of an entire sprint merely
learning about Minimax. Now, you can’t really learn a subject like an algorithm
without implementing it so it hasn’t been all reading, but I’m still not done. I
think I have it a lot of the way there, but it’s not unbeatable like it should
be. Throw in the extra step of writing it in F# and it’s a perfect storm of
being hard for me to understand how to implement it. I understand the concept of
what is trying to be done in Minimax, but getting that best move out of the
algorithm was so hard for me. I’ve stumbled my way through it so far, but I’ve
never wanted to punch a laptop screen as much as I have this week. I wanted to
have some insightful blog post this week, but my mind is so drained that I
thought I’d just vent and hopefully anyone else who is struggling with something
at work can feel a little less alone. Someday soon my Marley will be gone and
I’ll look back at how great it was. Until then it will continue to make my life
difficult. 

I know that for most people *Marley and Me* and is heartwarming tale of
friendship that shows us what true love is all about. Perhaps I am just
pessimistic. Lately I've discussed many times about whether the
frustration is part of the learning process or it's just annoying. I feel like I
have been learning quickly and I can't imagine a scenario where I would pick up
a functional language like I have if I was not thrown off the proverbial deep
end. It's been frustrating and I'm slightly disheartened, but I know that
there's always a peak to the mountain. Every time you conquer something and look
over your shoulder it never seems as daunting as when you started out and I know
that is the case with Minimax too. Even though my algorithm isn't finished I
have it roughed in. I know that I can now start out with my test cases and build
on them until it is refined into what I want. One day soon I'll look back and
forget all of the frustration and realize everything I learned in the process.
Maybe that was the point of the movie, or maybe Marley was just annoying. It
probably depends on if you're viewing it from the top or the bottom of the
mountain.