---
title: "Git Rebasing Simply Explained Part III" 
date: 2017-10-24T15:54:22-05:00
draft: false
---

Now we know why we should think about using a rebase, and we know what a rebase
is. It’s time for us to take a look at a real-world example. I’ve created a
small C#.NET project that you can fork and try out yourself. The repository is
located on my Bitbucket account
[here](https://bitbucket.org/kdenton/rebasing-training/overview). I could have
simply made a few files with some lorem ipsum paragraphs and done the same
thing, but it never feels the same to me. I like somewhat real-world examples
that you can imagine working with in a team environment. The premise of this
application is based off of a Forbes article you can read on their [website](
https://www.forbes.com/sites/frederickallen/2013/01/15/cat-beats-professionals-at-stock-picking/#78b732c5621a).
It tells the story of how a cat named Orlando beat a team of mutual fund
managers in 2012. The managers used their decades of combined experience with
the stock market to make investment choices, and Orlando used his favorite cat
toy thrown at a grid of numbers assigned to stocks. Further searching showed me
other examples of animals beating fund managers through random chance.

Actively managed funds generally come with much higher fees than index funds and
people like to use them because it’s comforting to think that someone with
experience is actively managing your money. Not only do these fees eat into your
earnings significantly, every year index funds beat the earnings of the majority
of actively managed funds. Most of the time you are losing money rather than
gaining by using actively managed funds. We’ve decided to write an application
for people who would rather not use index funds but don’t want to pay the fees.
What it does is download a list of stocks in the Nasdaq stock exchange and picks
ten random stocks to invest in for the year. Theoretically this should be just
as performant as paying someone to manage a fund for us.

> After forking the repository I listed above there are a few steps you need to
> do. First you are going to need to create a branch based on
> `training-my-branch`. I just name it `my-branch`. Next you'll need to make
> another branch based off of `training-someone-elses-branch`, which I just name
> `someone-elses-branch`. Next you'll want to merge `someone-elses-branch` into
> the `develop` branch. This will ensure that you have conflicts when you try to
> merge `my-branch` into `develop`. You can just use the training branches if you want,
> it's your fork and you can always just create another based on mine so do
> whatever works best for your

Right now, we are in the situation that I showed you in the first illustration
in the last part. Our `develop` branch had some commits in it which would
download the list of stocks and then display those to the console. In our
`my-branch` feature branch we are going to add a closing message so that people
know the program is done running and also randomly pick ten stocks from the
Nasdaq list. We also decided that we didn’t like the way one of our classes was
implemented so we abstracted out a stock manager interface and implementation to
make it clearer. We have done this and the work has taken us four commits to
accomplish. While we were doing this though, someone else on the team, on the
branch `someone-elses-branch`, has merged a commit into the `develop` branch.
This results in us having a *forked history*.

We’re finally done with our feature work; all of our unit tests pass and the
thing runs. Everything is great from our standpoint so we go through our normal
process of pushing our changes up to the remote repository and open a pull
request. When we go to create this pull request though we realize that something
must have been merged into `develop` while we were working because we are
getting merge conflicts. Now it’s time to use our rebasing skills to fix this
problem. The first thing we’re going to want to do is a fetch to make sure we
have the latest repository history. After that we’ll switch to our `develop`
branch and pull any changes down. It would look something like this.

![image4](/images/git-rebasing/image4.png)
 
> Commands Run:

> `git push origin my-branch` (push up our latest changes to the remote repository)

> `git fetch` (get the latest repository history)

> `git checkout develop` (checkout our develop branch locally)

> `git pull` (pull down the latest changes for the develop branch on the remote repository)

> `git status` (check the status of our local branch to make sure it is up to date with the remote)

So now we’ve updated our shared `develop` branch to its current status on the
remote repository and incorporated that one commit that has happened since we
started on `my-branch`. We’re now ready to start with the rebase. Remember that
I said that we rebase *onto* other branches so we are going to want to checkout
`my-branch` again. Next, we will begin the rebase by typing `git rebase
develop`. So, we have our `my-branch` checked out and we are telling Git to
rebase `my-branch` onto the `develop` branch. This will start applying the first
commit in `my-branch` onto the tip of our local `develop` branch. 
 
![image5](/images/git-rebasing/image5.png)

> Commands Run:

> `git checkout my-branch` (switch back to `my-branch`)

> `git rebase develop` (start rebasing `my-branch`’s commits onto `develop`

We’re presented with some merge conflicts. These are part of the merge conflicts
that we saw when we tried to create our pull request. Git outputs all of this
text and it can look a little intimidating at first, but it’s really just saying
that there are two files that were modified by both `my-branch` and `develop`
and Git needs help in deciding whose changes are correct. The first thing we
will want to do is run the `git status` command to get a better picture of what
is going on. Before we do that, I want to point out one thing though. In the
bottom right hand corner of the screenshot you will see `(my-branch|REBASE
1/4)`. This is basically what step of the rebase we are on. Because we have four
commits in our branch we are applying the first of four commits onto the tip of
our local `develop` branch. You will not always have to resolve conflicts if Git
is able to figure out how to apply a commit on its own. You may make it to
commit `3/10` before having to resolve a conflict if you have ten commits. If
you’re extremely lucky you may not have to resolve any. Depending on how many
commits you have and when you need to resolve conflicts your steps will look
different each rebase. Now, let’s look at the status of our branch.

![image6](/images/git-rebasing/image6.png)
 
> Commands Run:

> `git status` (check the status of the branch)

We have a couple of colors going on here. Green colored files mean that Git was
able to figure out how to apply those changes to the file without needing any
intervention on our part. The red colored files mean that there were changes in
both `my-branch` and `develop` to those specific files and Git needs our help to
figure out whose code is correct. Looking at the output we can tell that we will
need to make changes to the `IDisplay.cs` and `DisplayOnConsole.cs` files. In
the next part we will take a look at those files and figure out how we decide
what to do.
 


[Previous Page](/blog/git-rebasing-simply-explained-part-ii/) | [Next Page](/blog/git-rebasing-simply-explained-part-iv/)