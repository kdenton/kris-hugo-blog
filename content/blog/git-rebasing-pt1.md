---
title: "Git Rebasing Simply Explained" 
date: 2017-10-24T15:52:22-05:00
draft: false
---

This is a follow-up to a live presentation that I had the opportunity to do
recently. The premise of the talk was to explain what Git rebasing is and
provide some information on the benefits of using rebasing over merging. Since
the audience were all my colleagues it was a good chance to explain the
situation as I see it and how it would benefit our workplace. I would like you
to notice that this blog series is called *Git Rebasing Simply Explained*.
Simply does not necessarily mean quickly and this series is quite long. I split
it into multiple parts and I am going to start with why I feel you should use
rebases over merges. Then I’ll explain rebasing in a visual way and then end the
series with a real-life walkthrough of what a rebasing workflow looks like. I
do, in fact, realize that Git users online seem to be split almost 50/50 on
whether merging or rebasing is better. Around the office we have about 25% of
developers who use rebases regularly and the rest use merges. I feel like a lot
of the resistance to rebasing can be split into a few camps: fear of force
pushing, not trusting co-workers to resolve conflicts, and those that just think
it’s more work.
 
The first camp is easy to address. People fear force pushes because they
overwrite everything on the remote branch with what you have on your local
branch. I can understand the hesitation, but they aren’t inherently bad in any
way. Force pushing is something that’s necessary and there’s a reason that it
exists as a tool for you to use. You’re going to need to do it from time to time
anyways and once you get comfortable doing them then it’s not a big deal. A side
benefit is that they *force* (get it?) you to really think about what you’re
doing, which is always a good thing. It’s inevitable that you are going to make
changes on your local branch that will get you out of sync with the remote.
Force pushing is how you fix this and it’s best to get comfortable with them. It
is very important to note that you should not be force pushing to shared
branches, and whoever maintains the repository should not allow force pushes on
branches like `develop` or `master`. Only force push to your feature branch and
then create a pull request to get that feature branch merged into the shared
branch.

The second camp is one that I’ve actually heard and read from multiple people
and one that I find intriguing and also a little funny. Because rebasing
rewrites the history of the repository it naturally implies a level of
competency by the person making the decision on what code stays and what code
changes when resolving conflicts. Some people have a hard time believing that
everyone will have the ability to choose correctly in these situations when that
conflict happens and the person doing the rebase is unfamiliar with the changes.
Because the rebase rewrites history it can be difficult to get back to exactly
what happened in that situation, because you don’t have that merge commit. 

I see it from the exact opposite viewpoint though, I think rewriting the history
and rebasing *makes* you be more deliberate and methodical in your conflict
resolution. You have an opportunity to really understand the things that other
people are writing and how that fits into the what you’re writing. You *know*
that you are going to have to force push your decisions back to the originating
branch so you need to understand exactly what is going on. This fosters
conversations with the other people who are committing to the same branches you
are. I’ve seen it happen where a rebasing conflict resolution question brings up
a whole slew of questions about the viability of a feature in its current
design. Those are things you’d rather figure out sooner than later. 

The third camp is one that I’ve never truly heard a great answer on what exactly
the difficulty is, just that it is harder. Rebasing isn’t really any harder than
merging, and conceptually you are in exactly the same place at the end that you
would be with a merge. Sometimes people that have branches with a lot of commits
that make changes to the same areas of code can feel like they are resolving the
same conflicts over and over again. Unlike a merge, which obviously merges two
branches together, a rebase replays your commits onto the branch you are
rebasing onto. So, all of those similar commits will get replayed one by one,
and they will all probably have to be resolved. I can’t really speak to this
because part of your rebasing workflow should be that you squash your related
commits into one using an interactive rebase. You shouldn’t have all of those
similar commits anyways.  Other than that, I just cannot see what is harder
about it. You have to resolve conflicts with a merge too, I just don’t get it.

Now that we’ve cleared the air on some of the more common disparaging remarks
you hear pointed at rebasing, I want to tell you about the benefits of rebasing.
The first, and most important, is that you are dealing with a single commit when
you merge back into the originating branch. If there is a bug in your code, or
you changed a config file in a way that causes issues with your infrastructure
you can easily pluck that commit back out with a revert. When using merges, it
is much more difficult to get that problem code back out of the repository. The
second is that those merge commits are unnecessary pollution in the repository
history. Like I mentioned above, whether rebasing or merging you are getting to
the exact same point in the end. Why have all of that unnecessary history to
parse through if rebasing gets you to the same place. Another benefit is that
your branch visualizations don't look like a bowl of spaghetti when using
rebases like it does with merges. You can tell that someone made a branch, made
their changes, and merged back in, which is all that really matters.

So now you’re armed with some information on rebasing and you can make a choice
on what makes sense for you. Sometimes you may not have an option, your
workplace may force you to use merges. You may also have just been doing merges
forever and you don’t care to change now. That’s fine, it’s much more important
that you be comfortable with whatever choice you make than that you use one way
over the other. I just feel that rebasing has gotten a bad reputation and it’s
not deserved, especially given how useful it is. The rebasing versus merging
conversation is one of those rare times when you’re going to be just fine if you
do it either way. Both ways are perfectly valid, I just think that rebasing has
an edge. Next, I’m going to try to explain Git rebasing in an easy to understand
way with some visualizations. Go to the next page to continue reading about
that.

[Next Page](/blog/git-rebasing-simply-explained-part-ii/)