---
title: "Spaghetti Code Recipe"
date: 2017-10-06T13:37:45-05:00
draft: false
---

![Spaghetti](/images/spaghetti-code-recipe/spaghetti-sign.jpg)

<span style="font-weight:400;">My grandmother has that stereotypical gigantic stack of cookbooks in her kitchen, and I think there is a million recipes in there. Generally I see her make the same ten things though, so who knows what the point is. I don’t want to have that stack of recipes. The lesson I’ve learned from my grandmother is that you only need a few good ones. I’ve decided to make my own recipe book and to get it going I wanted to create one that will be used all around the world by many people. I have a winner here and I’m willing to put it up against the best because I know mine will win out most of the time. Try it out and let me know how it is.</span>

<h2 style="text-align:center;"><b>Spaghetti Code</b></h2>

<b>Main Ingredient:</b><span style="font-weight:400;"> The Backlog</span>

<b>Other Ingredients:</b>

<ul>
    <li style="font-weight:400;"><span style="font-weight:400;">Cleverness</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">Forced Implementations</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">Big Functions</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">In Line SQL Statements</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">Comments</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">Incomprehensible Variable Names</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">Resistance to Change</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">Half-Enforced Standards</span></li>
    <li style="font-weight:400;"><span style="font-weight:400;">Unintended Consequences</span></li>
</ul>

<b>Directions:</b>

<span style="font-weight:400;">So, to start out, we’re going to need a backlog, and not like a little backlog either. We’ve got a lot of application to feed here, so let’s make sure we start out the right way. We need to have so many features, enhancements, and bugfixes coming up that it’s not reasonable that we’ll ever get through it all. The main reason I love this recipe is that we can just keep adding to the backlog so that we have a never-ending supply of spaghetti code! To go along with that, I consider cleverness to be the tomato paste here, and everyone knows the true art of spaghetti is the sauce. Just like that little old lady walking away from the judging table at the local fair with her blue ribbons for best sauce, I walk away from my code with my own kind of ribbons. Some of my favorites are multi-line <code>​ternary statements</code>​ with at least ten symbols in them, weird magic string <code>​dictionaries</code>​, and that one <code>​class</code>​ I made a bunch of other ones implement in order for them to work for absolutely no reason. It’s a badge of honor for me. I spent ten minutes figuring out how to not use an <code>if statement</code> on this line because I’m not a barbarian. The least you can do is spend five minutes figuring out the meaning of what I wrote.</span>

<span style="font-weight:400;">Tomatoes alone make for a poor sauce though so we’re going to need to spice it up. By spice it up I mean that tomatoes are a little too much for me sometimes so I like to bland it up a bit by adding comments to everything. Variable declaration that actually has a decent name? You get a comment. Got a <code>getter function</code> that returns a value? Better believe I’m throwing a comment in there explaining what that one liner is doing. I’m going to comment this thing up so much you’re going to miss the one comment I put in there that really means something. That’s just how bland I like my sauce. Now I’m not totally uncouth, I like to have some spices in there but I just like it to be a little bit. That’s why I also like to make my important variables be three characters or less. See, it’s all about minimalism. A variable named <code>invoiceTotalBalance</code> is all in your face, you can’t help but taste it. True culinary art is about making the person not even realize that they are tasting that spice. This is why I like to increment a variable named <code>b</code> in a three-layer nested <code>for loop</code>. That way, you’re not quite sure what’s going on, but you know something is.</span>

<span style="font-weight:400;">Now that we’ve got the sauce going on its own over there, time to get started on that garlic bread. It’s a given that people eat a lot of garlic bread with spaghetti so we’ve got a big job ahead of us. What’s interesting to consider is what if we had an oven that is so huge we could cook any number of trays of garlic bread in it at the same time. Think of how easy that makes everything. I’ve got you covered though because this is my guiding principle when writing functions too. You know what’s easy to read? Small functions. You know what’s boring? That’s right, it’s small functions. I won’t apologize for not being boring, which is why that critical function is over four thousand lines long. Go ahead and tell the customer we can’t change how it works, either that or you can accept that our team will be refactoring it for the next two months. How not boring is that?</span>

<span style="font-weight:400;">Of course, garlic bread without garlic is just bread. It’s pretty obvious we’re going to need some. Luckily, I have a plethora of my favorite kind of garlic for our bread, and it’s a little-known species of the vegetable called </span><i><span style="font-weight:400;">Inline SQL Statements</span></i><span style="font-weight:400;">. I don’t know why more people don’t use it. The secret here is you must have your bread going really well, preferably at least five hundred lines of code. Then you just liberally sprinkle </span><i><span style="font-weight:400;">Inline SQL Statements</span></i><span style="font-weight:400;"> all over that bread. Just everywhere. Preferably you’re going to make multiple joins on weirdly related tables and just pull out like fifty columns worth of data. Then we’re going to one by one shove this data into an <code>array</code>. This is my favorite part of the recipe, and I know my mouth is watering just thinking about it, because I never comment that <code>array</code>. Never. If you’ll remember, I told you I like to save my comments for variable declarations.</span>

<span style="font-weight:400;">So we’ve got our noodles, sauce, and garlic bread done. Now it’s time to bring it all together. The secret to getting this to turn out perfectly is to have a bunch of standards that we enforce sometimes, but not always. I leave people guessing about how much I actually care about the standards that I helped come up with. This isn’t McDonald’s, this is fine cuisine. It’s an artform and I’m not going to force myself to follow rules when it stifles my creativity. That’s it ladies and gentlemen. This meal is the perfect blend of inability to change and things being affected that nobody could have predicted. You’re going to love it when you try it.</span>

<hr />

<span style="font-weight:400;">Like I said in the beginning, I’ve got a recipe book to make. Watch for more to come, and of course you can use any of these that you want in your own kitchen.</span>

<span style="font-weight:400;">Also, I’m aware that it’s contested whether garlic is considered a vegetable, herb, or spice. Garlic belongs to the same genus as an onion, which I do consider a vegetable and is all I really need to know.</span>