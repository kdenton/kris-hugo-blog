---
title: ( Probably ) Everything You Need To Know To Set Up Continuous Integration Using Jenkins/Bitbucket And A Java/Maven Project
date: 2017-10-06T13:37:29-05:00
draft: false
---

Continuous Integration, it’s so powerful and man is it awesome. I’ve also recently learned it’s a huge pain in the rear to get set up when it’s your first time and you have no idea what you're doing. For anyone who doesn’t know, continuous integration, or simply CI, is a system where developers are merging code to a project repository and these merges kick off automated builds which can then run the unit tests for that project. The benefit of CI is that you constantly know the state of your repository and its branches, the developer gets concrete, real-time verification that things are working and potential problems can be more easily identified. I was having a hard time finding information that had everything I needed to integrate my Java/Maven project with a Bitbucket repository using Jenkins to build it. During those times when you’re frustrated with trying to accomplish something you often think, “I wish someone made a tutorial about this.” I had that same thought about 10 minutes into my attempt and decided to do something about it. Below is (probably) everything you will need to know to get your own continuous integration environment set up.

To begin, I am going to assume that you have Jenkins installed on a server somewhere, you can access it in a terminal window, you have a `jenkins` user, and you have installed the Bitbucket and Git plugins. Another plugin that we will be using and will need to be installed is called Bitbucket Build Status Notifier. While this plugin isn’t strictly necessary for Jenkins to do its thing, it provides a really nice icon in Bitbucket that will show you the build status at a glance. You may be thinking, who is this Jenkins? At its core it’s an open source server application that will automatically build, test, and deploy your software. Which as a developer you’re probably familiar with all of these; you’ve for sure built an application, you probably run your own unit tests, and deployed your projects. Jenkins is just a helpful little guy who does these things for you so you don’t have to. Bitbucket is what it says it is, a bucket to store your bits in. The bits being your source code or even something as simple as a single text file. Bitbucket works with the popular version control systems Git and Mercurial. So, let’s get started.
<blockquote>If you're already confused and aren't sure how to proceed, DigitalOcean has a great tutorial on how to install Jenkins on an Ubuntu linux server. If that's the case then before you continue reading, click <a href="https://www.digitalocean.com/community/tutorials/how-to-install-jenkins-on-ubuntu-16-04">here</a>.</blockquote>
***

# Jenkins

First things first we’re going to want to open a terminal window to our Jenkins server, PuTTY is a popular SSH client for Windows and what I use. Next, we will be generating some SSH keys, and since our server is running Linux we will do that by first switching to our `jenkins` user by using the `su – jenkins` command.
<blockquote>As a quick aside, the <strong>jenkins</strong> user is a user that doesn’t have log in permission but you can <strong>su</strong> to that user. CI will run its commands using the <strong>jenkins</strong> user and since that user needs to be able to pull the repositories when it is performing a job you need the SSH keys to be generated in <strong>jenkins</strong> user’s home folder.</blockquote>
Now we need to generate these keys by using the `ssh-keygen` command. This will generate a set of SSH keys for you, one is private and one is public. Whenever you run this command it isn’t required that you add a passphrase to generated keys, but I would recommend it. The extra security is worth it and it won’t cause the process to be any more painful than it already is.
<blockquote>The easiest way to think of them is that the private key is the lock and is kept on the resource and the public key is the…well key I guess, and is locally stored on the computer/server of whatever is trying to access the resource. They must match up in order to be validated and this is more secure that the typical username/password combination.</blockquote>
We then use the `cat` command to display the generated public key that is located in our ` /var/lib/jenkins/.ssh/id_rsa.pub` file. This will display the contents of that file and is the public key that we are after so we can provide it to Bitbucket. Bitbucket will need the key to validate it against the lock (private key) on the server in order to gain access.

*Not the actual private key on my server by the way, nice try hackers!*
<img class="alignnone size-full wp-image-45" src="/images/java-maven-bitbucket/jenkins_terminal.png" alt="jenkins_terminal" width="1024" height="1016" />

Go ahead and copy the output of the public file and paste it in a text editor, we’re going to need it in a minute. Also, you need to include the `ssh-rsa` at the beginning of the file but do not keep the `jenkins@whateverdomain` that is appended on the end, that isn’t part of the key.

***

# Bitbucket

Now we’re going to move on to the Bitbucket part for a while. I’m not going to walk you through how to create a Java/Maven project and get it in Bitbucket, that’s out of the scope of this tutorial and there’s plenty of resources online that can show you those things. We are going to assume that we have a project that contains one test that asserts that true is equal to true. We’ll also assume that the project lives in a repository in your Bitbucket account and you have read/write access to that repository. Now, on to the good stuff.

After logging into Bitbucket, go to your Bitbucket profile and navigate to the **Settings** page. What we’re looking for is this area:

<img class="alignnone size-full wp-image-48" src="/images/java-maven-bitbucket/security_menu.png" alt="security_menu" width="279" height="243" />

Click on the **SSH keys** option in the menu and add a new SSH key from that page. This is where we are going to place the public key that we copied out of Jenkins terminal a minute ago and go ahead and label it **Jenkins**.

Next, we’re going to go to the repository that we have set up with our Java/Maven project with the one test in it. Go to the settings for that repository and look for the Workflow section and the **Webhooks** option.

<img class="alignnone size-full wp-image-51" src="/images/java-maven-bitbucket/workflow_menu.png" alt="workflow_menu" width="263" height="222" />
<blockquote>I want to point out that you may have noticed there is an <strong>Access keys</strong> option under the General section in the settings. That’s right, you can provide your public SSH key on a per repository basis. But because we trust ourselves and maybe want to have many different repositories that have access to Jenkins without setting up the SSH key each time we will put the public key at a user level.

If you’re wondering what a webhook is, it is a way for a third-party service like Bitbucket to make requests to your CI server. Which makes sense since we need Bitbucket to kick off a Jenkins build whenever a merge happens in our repository (the event).

To read more about Bitbucket’s webhooks and when to use them, click on this <a href="https://confluence.atlassian.com/bitbucket/manage-webhooks-735643732.html" target="_blank" rel="noopener">link</a>.</blockquote>
From here we’re going to add a new webhook, go ahead and populate it like this:

<img class="alignnone size-full wp-image-53" src="/images/java-maven-bitbucket/webhook_setup.png" alt="webhook_setup" width="663" height="487" />

Give it whatever title you want and the URL can either be the IP Address and port number of your Jenkins server or the URL to that server if it will resolve to one. If you have a URL, make sure you include the `http://` or `https://` in your URL. After the server URL you will need to add `/bitbucket-hook/`, then leave the status as Active and the Trigger as Repository Push and we’re good to go with our webhook.
<blockquote>If you’re wondering where in the heck that <strong>/bitbucket-hook/</strong> part of the URL came from, the Bitbucket plugin that I assumed was installed on the Jenkins server at the very beginning provides us with that easy access point.</blockquote>
For the last part of our Bitbucket portion we are going to go back to the user profile and settings page. We now need to generate an OAuth consumer key so that the Bitbucket Build Status Notifier plugin we will be using can securely access our Bitbucket repositories. You will be looking for the Access Management section of the settings and the **OAuth** option.

<img class="alignnone size-full wp-image-56" src="/images/java-maven-bitbucket/access_management_menu.png" alt="access_management_menu" width="268" height="220" />

Here we are going to add a consumer for our Bitbucket repositories. Fill it out like the following:

<img class="alignnone size-full wp-image-58" src="/images/java-maven-bitbucket/oauth_consumer_setup.png" alt="oauth_consumer_setup" width="757" height="700" />

The callback URL is either the IP Address and port number for your Jenkins server or its URL. There is no trailing slash on the Callback URL. Again, if you are using a URL instead of the IP Address and Port number, make sure you include the <code>http://</code> or <code>https://</code> with the URL. Make sure the **This is a private consumer** option is checked and that the OAuth consumer has both read and write access to the repositories.

<img class="alignnone size-full wp-image-60" src="/images/java-maven-bitbucket/permissions.png" alt="permissions" width="727" height="356" />

Once you save your new consumer setup, Bitbucket will generate a **Key** and **Secret** that we will use on our Jenkins server. Keep track of these as you’ll need them a little later. That’s it for the Bitbucket part.

***
# Jenkins Web Console

Now let’s move to our Jenkins web console. Just type the URL for the Jenkins server in your browser and you’ll be greeted with something like this:

<img class="alignnone size-full wp-image-63" src="/images/java-maven-bitbucket/jenkins_console_login.png" alt="jenkins_console_login"/>

When Jenkins was initially set up on the server, it generated an admin user and a random password. The file that contains this random password is located at ` /var/lib/jenkins/secrets/initialAdminPassword`. Use the username `admin` and the password from that file to log in.

We need to set up a couple of credentials now, one is for our Bitbucket account. We will be providing the lock (private key) portion of the SSH key pair so that Jenkins knows that our Bitbucket public key is allowed to access the server. The other credential we need to set up is so that our Bitbucket Build Status Notifier plugin is also able to be verified. As you may be noticing, that Jenkins guy doesn’t necessarily have the best interface, the good news is that it is getting better all the time, but you’re about to see a particularly annoying route to accomplish something in a second.

The menu on the left-hand side should look basically like this:

<img class="alignnone size-full wp-image-65" src="/images/java-maven-bitbucket/jenkins_console_menu.png" alt="jenkins_console_menu" width="257" height="423" />

What we are looking for is the **Credentials** option in the menu. Go ahead and click on that and then spend a few minutes trying to figure out how to add a new credential. Go ahead, I’ll wait here. If you didn’t figure it out, here’s how you do it. You should be taken to a page that looks like this after clicking on the **Credentials** menu option:

<img class="alignnone size-full wp-image-67" src="/images/java-maven-bitbucket/jenkins_console_credential.png" alt="jenkins_console_credential" width="1323" height="437" />

You won’t have any credentials set up yet though and would probably like to do so. What you have to do is click on that (global) link with the icon next to it. That will take you to another page that has a menu option to add a credential. Just not really apparent at first glance, but I digress. So now you can click on the **Add Credentials** menu option which will take you to a page that allows you to do just that. The kind of credential you are adding is **SSH Username with private key** because why would we not? We went through all of that trouble to generate our key pair. Go ahead and fill out the window like this and I’ll explain the options a little bit:

<img class="alignnone size-full wp-image-69" src="/images/java-maven-bitbucket/jenkins_console_credential_setup1.png" alt="jenkins_console_credential_setup1" width="606" height="468" />

The **Kind** is just specifying that we want to be secure and use our SSH keys that we generated rather than using our Jenkins username and password or putting a file somewhere on the server with our secret. The **Scope** needs to stay global so we can have our run of the server. The **Username** is our `jenkins` username that we set up or was generated for our Jenkins server. Since he’s the guy doing all of the work for us we need to identify him. The **Private Key** is the other half of the SSH key pair we generated. You can get it by running the same `cat` linux command that you used to get the public key. The only thing to change is that rather than opening the public file at ` /var/lib/jenkins/.ssh/id_rsa.pub` you will be opening the other generated file at ` /var/lib/jenkins/.ssh/id_rsa`.
<blockquote>As a side note if you’re not too familiar with SSH keys you will need the <em>BEGIN and END RSA PRIVATE KEY</em> identifiers that kind of look like fluff in the file.</blockquote>
The only other things we need to worry about is that the **Passphrase** being requested here is whatever passphrase you set up for your SSH keys if you did what I told you to and decided not to be yet another unsecure server. You can either give the credential an **ID** or if you leave it blank, if you leave it blank then Jenkins will generate a GUID for the **ID**. The **Description** can be left blank but for this demonstration we’ll just call it Build Host. That’s it for the Build Host credential, go ahead and save it.

Now it’s time to add the credential to our Jenkins server for the Bitbucket Notifier plugin. Go through the same process except this time we are going to use the **Username with password** as our **Kind**. Fill out your new credential form like this:

<img class="alignnone size-full wp-image-72" src="/images/java-maven-bitbucket/jenkins_console_credential_setup2.png" alt="jenkins_console_credential_setup2" width="654" height="420" />

This one’s a little simpler, but let's make sure we understand what we are doing. The previous credential used our SSH keys because that's how we've chosen to provide authentication to the server. This credential will be using a username and password pattern because that is what is required by the Bitbucket Build Status Notifier plugin that we assumed was installed. If you remember we have a **Key** and a **Secret** that got generated when we went through the Bitbucket OAuth setup. Your generated **Key** that you received will be the **Username** and your **Password** will be the generated **Secret**. Go ahead and leave the **ID** blank again as this will be populated with a GUID by Jenkins and give it a description of Bitbucket Notifier. That’s it for this one, save it and we’ll continue with the last part of this.

Back at the main Jenkins page, you’ll be looking for the **New Item** menu option.

<img class="alignnone size-full wp-image-65" src="/images/java-maven-bitbucket/jenkins_console_menu.png" alt="jenkins_console_menu" width="257" height="423" />

Click on that and you’ll be taken to a page that is where we will finally get to setting up our Java/Maven build process. You’re going to want to give the item a name and then use the **Maven Project** option.

<img class="alignnone size-full wp-image-76" src="/images/java-maven-bitbucket/jenkins_console_new_item.png" alt="jenkins_console_new_item" width="1312" height="365" />

You’ll now be presented with what seems like a pretty daunting amount of configuration just in order to get this god-forsaken thing to start working. Don’t worry though, we can and will ignore most of this and just fill out a few things. Everything we’ll be changing will be on the **General** tab and our first point of interest is the **Source Control** section. This is where we are going to connect Jenkins to our Bitbucket repository. You’ll want to fill it out something like this (don’t worry about the red text, that’s for explanatory purposes):

<img class="alignnone size-full wp-image-79" src="/images/java-maven-bitbucket/jenkins_console_setup_source_code.png" alt="jenkins_console_setup_source_code" width="1442" height="695" />

First things first, the **Repository URL** is exactly what it says, it is the URL to your Bitbucket repository. In Bitbucket, if you go to the home page for the repository you are wanting to build with Jenkins, the URL is at the top of the page. For the **Credentials** you are going to choose the `jenkins` credential with the Build Host description that we just set up. The second thing we want to notice is the `*/whateverbranchname`. Here is where we are going to specify the branches that we want Jenkins to build for us. In this example I have chosen my `develop` and `master` branches. What this ends up meaning is that any time there is a merge to either `develop` or `master` branches in our Bitbucket repository then Jenkins will work its build magic. At this point, if you don’t get a bunch of red text like the picture above then pat yourself on the back, you’re doing great! Now, if you are getting red text like that there is a few things that we can check out.
<blockquote>First, read the text. If, like in this case, it’s saying that the repository doesn’t exist, then make sure that it does in fact exist. Also, while you’re in Bitbucket we are going to make sure you have set up everything correctly. The next point of failure we’re going to want to look at is our SSH keys. In your user settings make sure that you did add that public key to your Bitbucket SSH keys. Make sure that your public key begins with `ssh-rsa` and that you included all of it. If that doesn’t fix the problem go back to the Jenkins web console and make sure that your credential that you set up for Jenkins has the proper private key. If you are still having problems then you’re going to have to figure it out because I’m all out ideas man. I'm just kidding, just leave a reply to this post and I'll try to help you through it.</blockquote>
Moving on, our next stop is the **Build Triggers** section. Here we’re going to want to make sure that the **Build when a change is pushed to Bitbucket** option is checked. This will trigger the build when we do our merges.

<img class="alignnone size-full wp-image-82" src="/images/java-maven-bitbucket/jenkins_console_setup_build_triggers.png" alt="jenkins_console_setup_build_triggers" width="663" height="290" />

Now we need to configure our **Build Environment**. There’s a few things that we’re going to want to turn on just for our own sanity. Set these options to the following:

<img class="alignnone size-full wp-image-84" src="/images/java-maven-bitbucket/jenkins_console_setup_build_environment.png" alt="jenkins_console_setup_build_environment" width="503" height="466" />

The **Delete workspace before build starts** is just going to give us a clean slate with each build at the expense of just a little bit more time. **Abort the build if it’s stuck** let’s Jenkins abort itself after timing out for 15 minutes, which is probably a good thing. It for sure shouldn’t take that long for our little project and you can’t always keep an eye on the build status. Last but not least, we’re going to turn on **Add timestamps to the Console Output**<em>.</em> This is just a good thing to do for debugging purposes, you’ll thank yourself later.

The next step is to make sure that **Run regardless of build result** is turned on. We want Jenkins to notify our Bitbucket repository about the build status even if it doesn’t build, well really we want to know especially if it *doesn’t* build. Almost done, hang in there.

At the last, we are going to want to add a **Post-build Action**. The one we are looking for is called **Bitbucket notify build result**. This is why we want to make sure that we have the **run regardless of build result** option enabled. Go ahead and check all three checkboxes. This is also where we will use the other credential that we set up. Click on the **Advanced** button and then select our **Bitbucket Status Notifier** credential. Then just click apply and save.

<img class="alignnone size-full wp-image-111" src="/images/java-maven-bitbucket/jenkins_console_setup_post_build_actions1.png" alt="jenkins_console_setup_post_build_actions" width="756" height="319" />

Yeah…that’s about it. Super simple stuff. No problem at all. I totally didn’t cry at all trying to do it the first time. We want to test this out obviously so let’s add another test to our test file in our Java project. Make sure you are on the develop branch and then go ahead and do something crazy, like make an assertion that false is equal to false. Whatever floats your boat. Commit and push that change and then go to your Bitbucket repository in your browser and look at the branches for that repository. You should notice a clock looking icon next to the develop branch, this is great, Jenkins is doing it thing and building your Java/Maven project! After a short while if you refresh the browser that icon should be a green checkmark. And that is actually it. We now have real time notifications about the build status of our branches. Congratulations!

<img class=" size-full wp-image-91 aligncenter" src="/images/java-maven-bitbucket/bitbucket_green_check.png" alt="bitbucket_green_check" width="94" height="62" />

