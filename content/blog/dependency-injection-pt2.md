---
title: "Dependency Injection Simply Explained Part II" 
date: 2017-11-20T15:52:22-05:00
draft: false
---

I’ve read a lot about dependency injection online and the problems people have
with it. There’s one thing that’s always bothered me; those conversations aren’t
really about dependency injection. In the previous blog post, I showed you how
even the most basic application that is throwing around a few objects is using
dependency injection. Most people just don’t call it that. What people have a
problem with is determining dependencies ahead of time, not newing up an
instance of an object every time they need one, and unit testing. Yet, most
importantly, what seems to be the biggest hold up for people is the use of
abstractions. You read about a lot of people who think it’s ridiculous to not just
create a new object when you need it. So why not just do that? Well it’s almost
Thanksgiving here so let’s use some seasonal examples. 

Because I’m a nerd in the sense that I’m also a huge fan of history as well as
technology, let’s think about the original Thanksgiving. European settlers,
technology wise, were the pinnacle of achievement for their time and the
environment they were operating in. Much like our legacy applications they do
what they do amazingly. Ships that sail across oceans, check. Projectile weapons
that anyone can master in a matter of hours, also check. Industrial revolution,
textiles, and structured societies. Check, check, check. In the year 1610
there’s still English settlers starving to death in the settlement of Jamestown,
Virginia. How could this be? They had all of the tools, the manpower, and
willingness to succeed and yet they weren’t. It’s because they were all
implementation and no abstraction. Let’s take a look at how Thanksgiving, one of
the very foundations of our country, could have been different if only they had
abstracted out their survival. 

Let’s imagine that our settlers at Jamestown wanted to survive. They brought the
crème de la crème of the Old World with them. They brought wheat, beef, and
peas. It turns out though that Virginia is very wooded, which isn’t the best for
cattle. Also, wheat requires wide open spaces to really thrive and peas can only
be grown in small quantities and require a lot of labor. Let’s imagine that
their program looked something like this:

![image1](/images/dependency-injection-part2/image1.png)

![image2](/images/dependency-injection-part2/image2.png)

There’s nothing wrong with that, it’s a solid stack in the right environment.
The problem is we’re not in the Old World, we’re here in the New World. We’re
experienced though, we know how to farm, how to raise animals. We invoked
`Survive()` but it’s not working. How could we have made this easier on
ourselves? We could have abstracted things out a bit. Survival is something that
is universal, why limit ourselves to just the implementation that we have here
in the Old World? Why does our little settler program need to know how to
survive? It just needs to know that it has to perform that task. Maybe we could
have abstracted that out a bit to look something like this.

![image3](/images/dependency-injection-part2/image3.png)

Now we just have an interface, our abstraction, whose only concern is *what*
needs to happen rather than *how*. We can have an implementation of *how* for
the New World.

![image4](/images/dependency-injection-part2/image4.png)

![image5](/images/dependency-injection-part2/image5.png)

![image8](/images/dependency-injection-part2/image8.png)

Our settlement application can now ask where we want to settle and then provide
us with the correct implementation of the settlement that we need. Sure, we
could have simply newed up our class instances here. Something like this maybe?

![image6](/images/dependency-injection-part2/image6.png)

We made it to the exact same place and we didn’t have to use any of that
abstraction bologna or dependency injection. There’s a few problems with this,
and they’re pretty big ones. First one that comes to mind is that this is hard
to test. If we wanted to unit test our `Main()` method we should really only be
testing what it does, not the subsidiary concerns like `Survive()`. With this
second way that is impossible. When testing `Main()` it will invoke an actual
instance of `Survive()`. When we abstract the responsibilities out we can
provide fake implementations of the dependencies within a function to make sure
that they provide exactly what we want every time and verify they are called.
All we really care about in our `Main()` method is that `Survive()` is called,
we don’t need it to actually be called. 

Next would be that it’s not very extensible. With an example this small it’s a
challenge to really show how big of a problem this will be, but I’m sure many
people have experienced this in their career. When you use concrete classes it
is easy to tack on extra functions or properties that do a specific thing and
affect functionality. Suppose after a few years our settlement application has
had some user requested features so we add them on.

![image7](/images/dependency-injection-part2/image7.png)

Our two settlement types are very different internally, because surviving in the
Old World is different than surviving in the New World. Our program really
shouldn’t care though, it’s all survival and that’s what it should be concerned
about. With an abstraction we could have changed only the implementation to add
these features. Our `Main()` method wouldn’t have needed to change at all. If we
can change code in one place then we are much less likely to introduce bugs and
our refactors are easier to do. We could also add a Mars implementation and not
need to change any functionality in our program. I think we can all agree that
surviving on Mars is vastly different than anywhere here on Earth, but that’s
all details on how it’s done. We should only worry about *what* needs to happen.

Lastly, and most importantly, you’re not a code monkey. You’re a programmer, you
build applications and take a myriad of individual components and put them all
together to provide an output. Your skills and knowledge are invaluable and yet
so many people seem to *want* to spend all day refactoring a codebase because a
well-used function had an additional parameter added. You should respect
yourself and the value of your time enough that you want to have abstractions so
that when the inevitable changes are asked for you can merely extend what
already exists rather than change it. A well abstracted program can really cut
down on the cost to make changes to your application. Now that we have a few
reasons to use abstractions, in the next blog post we’ll take a look at
injecting some dependencies.



[Previous Page](/blog/dependency-injection-simply-explained/) | [Next Page](/blog/dependency-injection-simply-explained-part-iii/)