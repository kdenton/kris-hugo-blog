---
title: "Dont Forget Your Acceptance Tests" 
date: 2017-10-06T15:48:22-05:00
draft: false
---

![train](/images/dont-forget-your-acceptance-tests/train.gif)

Take a second to consider the train wreck (pun intended) of the system above.
The individual parts worked perfectly, but the system doesn’t work at all. A
train arrives, it turns on the safety equipment, then when the train leaves it
turns the safety equipment back off. Let’s pretend for a minute that I have a
degree in Railroad Crossing Safety from Thomas the Tank Engine University. My
analysis is that the sensor on the left-hand side fails. The sensor on the right
side correctly senses the train and turns on the safety equipment and then when
it no longer senses anything it turns them back off. You can imagine the code
for something like that might look like this, with `watchForTrain()` running in
a loop all day.

![image1](/images/dont-forget-your-acceptance-tests/image1.png)

If this was the case it’s not very fun. It’s a physical failure and has nothing
to do with the software. Although I would think that our program should have a
way to verify the integrity of our sensors and put the safety equipment down if
one failed. What do I know though, my Railroad Crossing Safety degree isn’t from
an accredited university. What I choose to believe is that it was designed this
way, the sensors work and all the different pieces of the system passed their
safety tests. So, let’s take a look at what went wrong with our railroad
crossing system that has a no star safety rating.  We can then decide how it
could be better.

In order for this to work with my vision, I have to change something. Even smart
designers miss things sometimes and I only went to school for railroad crossing
safety. To me it makes sense that I would put both sensors on the right side of
the crossing, one close to the crossing and one further away where it should be.
We didn’t have any logical thinking classes at my alma mater. Regardless, I
still don’t understand how this happened, I unit tested everything. I know the
methods that turn on the light and lower the arms work as expected. I verified
that when the sensors are tripped they appropriately call the methods that
handle when to turn the safety equipment on or off. Every `if statement` has a
unit test testing the different outcomes. It honestly can’t even be my fault.
Although, there is one thing I didn’t do and no amount of unit tests will help
with this scenario. I didn’t write an acceptance test.

***
>
> If you'd like to know more about the differences between the types of testing
> you can do, there is a great Stack Overlow answer on the subject
> [here](https://stackoverflow.com/questions/4904096/whats-the-difference-between-unit-functional-acceptance-and-integration-test).

***
Unit tests prove that the individual pieces of our system work. Acceptance tests
are where we bring all of those pieces together and exercise the system as a
whole. It’s generally a good idea to start with an acceptance test that outlines
what should happen as an entire feature or system. Then write unit tests until
that passes. Let’s take a look at a contrived acceptance test that we could have
written to prove that my sensors make no sense at all. 

Feast your eyes on the following test! Is this the best way to test a software
system that relies on distances from physical sensors, crossing locations, and
vehicles? I have no idea, probably not. It was a great exercise in thinking
about how I would accomplish this though and I had a lot of fun trying different
approaches. 

![image2](/images/dont-forget-your-acceptance-tests/image2.png)

(It’s kind of funny though that when I wrote that I decided three seconds of
time was plenty of warning for an approaching train.)

![image3](/images/dont-forget-your-acceptance-tests/image3.png)

You'll notice that the crossing is 500 meters down the track with `sensorOne`
and `sensorTwo` located at 510 meters and 800 meters respectively. This
simulates that they are both on the same side of the crossing. The train and car
both start 500 meters away from the crossing and advance at the same velocity of
10 meters per second. Once the train hits `sensorOne` we start recording how far
the car travels after that point before getting to the crossing. Because we know
the car's velocity we can then tell how long it took the car to reach the
crossing after the safety equipment was turned on. We then have an assertion
that the car had more than three seconds to react to the signals. Because
`sensorOne` was on the wrong side of the track, this assertion fails. The car
was actually hit by the train. If we moved our first sensor to the other side of
the crossing and far enough down the track we would get a passing acceptance
test.

This is like one of those math problems in school. There is a car and a train
travelling at the same velocity towards a railroad crossing. The crossing is the
same distance away from each. Can a barely educated railroad safety crossing
engineer prevent a horrible, fiery crash of death and destruction? The answer is
no, no he can’t. At least not in the current configuration. It’s a good thing I
have an acceptance test, because it’s a big red shiny beacon of this isn’t going
to work. Without having to let a train run over a school bus I can tell that I
need to do something different. By using only unit tests there is no way that we
could have known that both of our sensors being on the same side of the
crossing would invalidate the whole system. We don't want to rely on common
sense, so we use acceptance tests. 