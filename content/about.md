+++
date = "2017-10-11T13:19:25+08:00"
draft = false
title = "about"

+++

My name is Kristopher Denton and I'm a software developer at Software Solutions Integrated (SSI) in little ol' Shelbyville, IL. As someone with only a little over three years of professional experience in the industry I am always looking for learning opportunities and ways to expand my knowledge. Right now I'm participating in an awesome opportunity provided to developers here at SSI. I am able to take a hiatus from production work on our products and spend six months investing in myself though our apprenticeship program. So yeah, I'm pretty stoked about that. For my blog posts I try to write them in an easy to understand style that is at least semi-enjoyable to read. I'll be adding content to my blog regularly over the next few months and I hope you enjoy reading it!